﻿using Nop.Core.Data;
using System.Data.Entity.Infrastructure;

namespace Nop.Data.Migrations
{
    public class MigrationsContextFactory: IDbContextFactory<NopObjectContext>
    {
        public NopObjectContext Create()
        {
            var dataSettingsManager = new DataSettingsManager();
            var dataSettings = dataSettingsManager.LoadSettings();
            return new NopObjectContext(dataSettings.DataConnectionString);
        }
    }
}
